#!/bin/bash

if test "$(basename $PWD)" != "Decaydata"; then
    echo "Need to run in Decaydata/ directory."
    exit 1;
fi

cd ..

zip -r Decaydata.zip Decaydata/ --exclude Decaydata/.git/\*
mv Decaydata.zip master/HADRONS++/
cp -rp master/HADRONS++/Decaydata.zip master/share/SHERPA-MC/

if test -e Decaydata/.git; then
    mv Decaydata/.git deploy.sh.tmp.Decaydata.git
fi
rel-2-2-5/AddOns/Dir2Db Decaydata/
mv Decaydata.db rel-2-2-5/HADRONS++/
cp -rp rel-2-2-5/HADRONS++/Decaydata.db rel-2-2-5/share/SHERPA-MC/
if test -e deploy.sh.tmp.Decaydata.git; then
    mv deploy.sh.tmp.Decaydata.git Decaydata/.git
fi

cd -
echo "Finished successfully."
